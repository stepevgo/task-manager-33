package ru.t1.stepanishchev.tm.dto.request;

import lombok.Setter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public AbstractIndexRequest(@Nullable final Integer index) {
        this.index = index;
    }

}