package ru.t1.stepanishchev.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.dto.request.*;
import ru.t1.stepanishchev.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface ITaskEndpoint extends IEndpoint {

    @NotNull
    String NAME = "TaskEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @NotNull
    String NAMESPACE = "http://endpoint.tm.stepanishchev.t1.ru/";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        @NotNull final String wsdl = String.format("http://%s:%s/%s?WSDL", host, port, NAME);
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(NAMESPACE, PART);
        return Service.create(url, qName).getPort(ITaskEndpoint.class);
    }

    @NotNull
    @WebMethod
    TaskChangeStatusByIdResponse changeStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIdRequest request
    );

    @NotNull
    @WebMethod
    TaskChangeStatusByIndexResponse changeStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIndexRequest request
    );

    @NotNull
    @WebMethod
    TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskClearRequest request
    );

    @NotNull
    @WebMethod
    TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCreateRequest request
    );

    @NotNull
    @WebMethod
    TaskListResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListRequest request
    );

    @NotNull
    @WebMethod
    TaskListByProjectIdResponse listTaskByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListByProjectIdRequest request
    );

    @NotNull
    @WebMethod
    TaskShowByIdResponse showTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowByIdRequest request
    );

    @NotNull
    @WebMethod
    TaskShowByIndexResponse showTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowByIndexRequest request
    );

    @NotNull
    @WebMethod
    TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIdRequest request
    );

    @NotNull
    @WebMethod
    TaskRemoveByIndexResponse removeTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIndexRequest request
    );

    @NotNull
    @WebMethod
    TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIdRequest request
    );

    @NotNull
    @WebMethod
    TaskUpdateByIndexResponse updateTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIndexRequest request
    );

    @NotNull
    @WebMethod
    TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskBindToProjectRequest request
    );

    @NotNull
    @WebMethod
    TaskUnbindFromProjectResponse unbindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUnbindFromProjectRequest request
    );

    @NotNull
    @WebMethod
    TaskStartByIdResponse startTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskStartByIdRequest request
    );

    @NotNull
    @WebMethod
    TaskStartByIndexResponse startTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskStartByIndexRequest request
    );

    @NotNull
    @WebMethod
    TaskCompleteByIdResponse completeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCompleteByIdRequest request
    );

    @NotNull
    @WebMethod
    TaskCompleteByIndexResponse completeTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCompleteByIndexRequest request
    );

}