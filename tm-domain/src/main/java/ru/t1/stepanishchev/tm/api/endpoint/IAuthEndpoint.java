package ru.t1.stepanishchev.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.dto.request.UserLoginRequest;
import ru.t1.stepanishchev.tm.dto.request.UserLogoutRequest;
import ru.t1.stepanishchev.tm.dto.request.UserProfileRequest;
import ru.t1.stepanishchev.tm.dto.response.UserLoginResponse;
import ru.t1.stepanishchev.tm.dto.response.UserLogoutResponse;
import ru.t1.stepanishchev.tm.dto.response.UserProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface IAuthEndpoint extends IEndpoint {

    @NotNull
    String NAME = "AuthEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @NotNull
    String NAMESPACE = "http://endpoint.tm.stepanishchev.t1.ru/";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance(
            @NotNull final String host, @NotNull final String port
    ) {
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + NAME + "?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(NAMESPACE, PART);
        return Service.create(url, qName).getPort(ISystemEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserLoginResponse login(
            @WebParam(name = "request", partName = "request")
            @NotNull UserLoginRequest request
    );

    @NotNull
    @WebMethod
    UserLogoutResponse logout(
            @WebParam(name = "request", partName = "request")
            @NotNull UserLogoutRequest request
    );

    @NotNull
    @WebMethod
    UserProfileResponse profile(
            @WebParam(name = "request", partName = "request")
            @NotNull UserProfileRequest request
    );
}