package ru.t1.stepanishchev.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.model.Project;

public final class ProjectCreateResponse extends AbstractProjectResponse {

    public ProjectCreateResponse(@Nullable final Project project) {
        super(project);
    }

}