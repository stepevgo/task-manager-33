package ru.t1.stepanishchev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.dto.request.UserLogoutRequest;
import ru.t1.stepanishchev.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "logout";

    @NotNull
    private final String DESCRIPTION = "logout current user.";

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getServiceLocator().getAuthEndpoint().logout(new UserLogoutRequest());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}