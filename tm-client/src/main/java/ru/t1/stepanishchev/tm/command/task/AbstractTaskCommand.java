package ru.t1.stepanishchev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.api.endpoint.ITaskEndpointClient;
import ru.t1.stepanishchev.tm.command.AbstractCommand;
import ru.t1.stepanishchev.tm.enumerated.Role;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected ITaskEndpointClient getTaskEndpoint() {
        return getServiceLocator().getTaskEndpoint();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}